Simple bukkit plugin that lets you make clans


commands:
/clan
  join <clanname> = Join <clanname>, creates if <clanname> does not exist
  leave = Leave your current clan
  sethome = Set your clan's spawn, only if you are the leader
  home = warp to home
  promote = make someone else leader. Only works if you are already leader
NOTE:
/clan sethome and /clan home can be disabled from the config.yml

WARNING:
config.yml is used for persistance, do not edit things unless you want them to change ingame.