package com.github.DeRaptir;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.Plugin;

public class DeClanListener implements Listener {
    private Plugin plugin;

    public DeClanListener(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void chatListner(AsyncPlayerChatEvent event) {
        String playerName = event.getPlayer().getDisplayName();
        String message = event.getMessage();
        if (null == plugin.getConfig().getString(playerName + ".clan.name")) {
            event.setFormat(playerName + ": " + message);
        } else {
            String clan = plugin.getConfig().getString(playerName+".clan.name");
            if (null == plugin.getConfig().getString(playerName+".clan.color")) {
                event.setFormat("[" + clan + "] " + playerName + ": " + message);
            } else {
            String color = plugin.getConfig().getString(playerName+".clan.color");
                if (color.equals("black")) {
                    event.setFormat("[" + ChatColor.BLACK + clan + ChatColor.WHITE + "] " + playerName + ": " + message);
                } else if (color.equals("red")) {
                    event.setFormat("[" + ChatColor.DARK_RED + clan + ChatColor.WHITE + "] " + playerName + ": " + message);
                } else if (color.equals("gray")) {
                    event.setFormat("[" + ChatColor.GRAY + clan + ChatColor.WHITE + "] " + playerName + ": " + message);
                } else if (color.equals("gold")) {
                    event.setFormat("[" + ChatColor.GOLD + clan + ChatColor.WHITE + "] " + playerName + ": " + message);
                } else if (color.equals("green")) {
                    event.setFormat("[" + ChatColor.GREEN + clan + ChatColor.WHITE + "] " + playerName + ": " + message);
                } else if (color.equals("blue")) {
                    event.setFormat("[" + ChatColor.DARK_BLUE + clan + ChatColor.WHITE + "] " + playerName + ": " + message);
                } else if (color.equals("white")) {
                    event.setFormat("[" + clan + "] " + playerName + ": " + message);
                }
            }
        }
    }
}

