package com.github.DeRaptir;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.Array;
import java.util.Arrays;

public class DeClanCommand implements CommandExecutor {

    private Plugin plugin;

    public DeClanCommand(Plugin plugin) {
        this.plugin = plugin;
    }

    public String getClan(Player p) {
        if (null == plugin.getConfig().getString(p.getName() + ".clan.name")) {
            return null;
        }
        return plugin.getConfig().getString(p.getName() + ".clan.name");
    }

    public boolean isLeader(Player p) {
        if (null == plugin.getConfig().getString(getClan(p) + ".leader")) {
            plugin.getConfig().set(getClan(p) + ".leader", p.getName());
            p.sendMessage("There was no leader for " + getClan(p) + ", so you are now the leader");
            return true;
        } else if (plugin.getConfig().getString(getClan(p) + ".leader").equals(p.getName())) {
            return true;
        } else {
            return false;
        }
    };

    public void changeColor(String arg, Player p) {
        if (arg.equalsIgnoreCase("black") || arg.equalsIgnoreCase("gray") || arg.equalsIgnoreCase("gold") || arg.equalsIgnoreCase("green") ||  arg.equalsIgnoreCase("red") || arg.equalsIgnoreCase("blue") ||  arg.equalsIgnoreCase("white")) {
            plugin.getConfig().set(p.getName() + ".clan.color", arg.toLowerCase());
            plugin.saveConfig();
            plugin.reloadConfig();
            p.sendMessage(ChatColor.GOLD + "Your clan color is now: " + ChatColor.WHITE + arg);
        } else {
            p.sendMessage("Invalid color:");
            p.sendMessage("You can choose between: " + ChatColor.BLACK +  "black" + ChatColor.GRAY +", gray" + ChatColor.GOLD + ", gold" + ChatColor.DARK_GREEN + ", green" + ChatColor.DARK_RED + ", red" + ChatColor.DARK_BLUE + ", blue" + ChatColor.WHITE + ", and white");
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(ChatColor.DARK_RED  + "" + ChatColor.BOLD + "YOU NEED TO PUT AN ARGUMENT IN!");
            return false;
        } else if (args[0].equalsIgnoreCase("join")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.DARK_RED + "You must be a player to use this command");
                return true;
            } else {
                Player player = (Player) sender;
                if (args.length != 2) {
                    sender.sendMessage("Command follows form of: /clan join <clanname> No spaces!");
                    return true;
                } else {
                    if (args[1].length() > 5) {

                    } else {
                        plugin.getConfig().set(player.getName() + ".clan.name", args[1]);
                        sender.sendMessage(ChatColor.GOLD + "Welcome to " + ChatColor.AQUA + args[1] );
                        if (null == plugin.getConfig().get(args[1] + ".leader")) {
                            plugin.getConfig().set(args[1] + ".leader", player.getName());
                        }
                        plugin.saveConfig();
                        plugin.reloadConfig();
                        return true;
                    }
                }
            }
        } else if (args[0].equalsIgnoreCase("leave")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.DARK_RED + "You must be a player to use this command");
                return true;
            } else {
                Player player = (Player) sender;
                if (null == plugin.getConfig().getString(player.getName() + ".clan.name")) {
                    sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You are already not in a clan");
                } else {
                    String clan = plugin.getConfig().getString(player.getName() + ".clan.name");
                    plugin.getConfig().set(player.getName() + ".clan.name", null);
                    if (plugin.getConfig().getString(clan + ".leader").equals(sender.getName())) {
                        plugin.getConfig().set(clan + ".leader", null);
                        sender.sendMessage(ChatColor.DARK_RED + clan + " now has no leader");
                    }
                    plugin.saveConfig();
                    plugin.reloadConfig();
                    sender.sendMessage(ChatColor.GOLD + "You are no longer in a clan");
                }
            }
        } else if (args[0].equalsIgnoreCase("color")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.DARK_RED + "You must be a player to use this command");
                return true;
            }
            if (plugin.getConfig().getBoolean("leaderOnlyColor")) {
                if (!(args.length == 3)) {
                   sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Wrong format. Use: /clan color <name> <color>");
                } else {
                    if (isLeader((Player) sender)) {
                        Player newColorPlayer = Bukkit.getServer().getPlayer(args[1]);
                        if (newColorPlayer == null) {
                            sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Player does not exist");
                            return true;
                        } else {
                            if (!(getClan((Player) sender) == getClan(newColorPlayer))) {
                                sender.sendMessage("The target player must be in your clan.");
                            } else {
                                changeColor(args[2], newColorPlayer);
                            }
                        }
                    } else {
                        sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You are not this clan's leader!");
                    }
                }
            } else {
                if (args.length == 2) {
                    changeColor(args[1], (Player) sender);
                } else {
                    sender.sendMessage("Wrong format. Use: /clan color <color>");
                }
            }

        } else if (args[0].equalsIgnoreCase("sethome")) {
            if (plugin.getConfig().getString("clanHomes").equalsIgnoreCase("false")) {
                sender.sendMessage(ChatColor.DARK_RED + "This feature is not available on this server");
            } else {
                if (!(sender instanceof Player)) {
                    sender.sendMessage("You must be a player!");
                } else {
                    Player player = (Player) sender;
                    if ( null == plugin.getConfig().getString(player.getName() + ".clan.name")) {
                        sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You must be in a clan");
                    } else {
                        String clan = plugin.getConfig().getString(player.getName() + ".clan.name");
                        if (null == plugin.getConfig().getString(clan + ".leader")) {
                            sender.sendMessage("Your clan had no leader. You are now leader.");
                            plugin.getConfig().set(clan + ".leader", player.getName());
                            Location loc = player.getLocation();
                            String location = (loc.getWorld().getName() + "|" + loc.getX() + "|" + loc.getY() + "|" + loc.getZ());
                            plugin.getConfig().set(clan + ".home", location);
                            plugin.saveConfig();
                            plugin.reloadConfig();
                            sender.sendMessage(ChatColor.GOLD + "You have set " + clan + "'s home!");
                        } else {
                            if ((plugin.getConfig().getString(clan + ".leader")).equals(player.getName())) {
                                plugin.getConfig().set(clan + ".leader", player.getName());
                                Location loc = player.getLocation();
                                String location = (loc.getWorld().getName() + "|" + loc.getX() + "|" + loc.getY() + "|" + loc.getZ());
                                plugin.getConfig().set(clan + ".home", location);
                                plugin.saveConfig();
                                plugin.reloadConfig();
                                sender.sendMessage(ChatColor.GOLD + "You have set " + clan + "'s home!");
                                return true;
                            } else {
                                player.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You are not this clan's leader!");
                            }
                        }
                    }
                }
            }
        } else if (args[0].equalsIgnoreCase("home")) {
            if (plugin.getConfig().getString("clanHomes").equalsIgnoreCase("false")) {
                sender.sendMessage(ChatColor.DARK_RED + "This feature is not available on this server");
            } else {
                if (!(sender instanceof Player)) {
                    sender.sendMessage("You must be a player!");
                } else {
                    Player player = (Player) sender;
                    if (null == plugin.getConfig().getString(player.getName() + ".clan.name")) {
                        sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You must be in a clan");
                    } else {
                        String clan = plugin.getConfig().getString(player.getName() + ".clan.name");
                        if (null == plugin.getConfig().get(clan + ".home")) {
                            sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You must have set a home");
                        } else {
                            String location = plugin.getConfig().getString(clan + ".home");
                            String[] loc = location.split("\\|");
                            World world = Bukkit.getWorld(loc[0]);
                            Double x = Double.parseDouble(loc[1]);
                            Double y = Double.parseDouble(loc[2]);
                            Double z = Double.parseDouble(loc[3]);
                            Location locationHome = new Location(world, x, y, z);
                            player.teleport(locationHome);
                            player.sendMessage(ChatColor.GOLD + "Welcome back to " + clan + "'s home!");
                        }
                    }
                }
            }
        } else if (args[0].equalsIgnoreCase("promote")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("You must be a player!");
            } else {
                if (args.length != 2) {
                    sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Wrong format, must use: /clan promote <player new leader>");
                } else {
                    if (null == plugin.getConfig().get(sender.getName() + ".clan.name")) {
                        sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You must be in a clan");
                    } else {
                        if (plugin.getConfig().get(sender.getName() + ".clan.name").equals(sender.getName())) {
                            String clan = plugin.getConfig().getString(sender.getName() + ".clan.name");
                            if (null == plugin.getConfig().getString(clan + ".leader")) {
                                plugin.getConfig().set(args[1] + ".leader", sender.getName());
                                sender.sendMessage("You are the new leader of " + clan);
                            } else if (!(isLeader((Player) sender))) {
                                sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You must be the leader of your clan");
                            } else {
                                Player newLeaderPlayer = Bukkit.getServer().getPlayer(args[1]);
                                if (newLeaderPlayer == null) {
                                    sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Player does not exist");
                                    return true;
                                } else {
                                    plugin.getConfig().set(clan + ".leader", newLeaderPlayer.getName());
                                    sender.sendMessage(ChatColor.GOLD + "You have promoted " + newLeaderPlayer.getName() + " as leader of " + clan + "!");
                                    return true;
                                }
                            }
                        }
                        sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You must be in the same clan as the player being promoted");
                    }
                }
            }
        } else if (args[0].equalsIgnoreCase("chat") || args[0].equalsIgnoreCase("c")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("You must be a player!");
            } else {
                if (!(args.length > 1)) {
                    sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You must put in a message");
                } else {
                    Player player = (Player) sender;
                    if (null == plugin.getConfig().getString(sender.getName()  + ".clan.name")) {
                        sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "You must be in a clan");
                    } else {
                        String[] newArray = Arrays.copyOfRange(args, 1, args.length);
                        String message = StringUtils.join(newArray, " ");
                        Player[] online = Bukkit.getOnlinePlayers();
                        String clan = plugin.getConfig().getString(sender.getName()  + ".clan.name");
                        for (int i = 0; i < online.length; i++) {
                            String clanPlayer = plugin.getConfig().getString(online[i].getName()  + ".clan.name");
                            if (null == clanPlayer) {
                                return true;
                            } else if (clanPlayer.equals(clan)) {
                                online[i].sendMessage((ChatColor.ITALIC + "Clan Chat: " + ChatColor.RESET + "" + ChatColor.DARK_GREEN + player.getDisplayName() + ": " + message));
                            } else {
                                return true;
                            }
                        }
                    }
                }
            }
        } else if (args[0].equalsIgnoreCase("help")) {
            sender.sendMessage("Avaiable commands are:");
            sender.sendMessage("join = join a new clan");
            sender.sendMessage("leave = leave clan");
            sender.sendMessage("color = set color of clantag");
            sender.sendMessage("home = teleport to clan's home");
            sender.sendMessage("sethome = set clan's home *LEADER ONLY*");
            sender.sendMessage("promote = promote player to leader");
            sender.sendMessage("chat = send message to clan");
        } else {
            sender.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Try /clan help");
        }
    return true;
    }
}
