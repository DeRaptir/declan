package com.github.DeRaptir;

import org.bukkit.plugin.java.JavaPlugin;
import java.io.File;

/*
Coded by:
==============================================
___  _   _ _    ____ _  _ _  _ ____ ____ _  _
|  \  \_/  |    |__| |\ | |\ | |__| [__  |__|
|__/   |   |___ |  | | \| | \| |  | ___] |  |
==============================================
 */

public class DeClan extends JavaPlugin {

    public static DeClan instance;

    public DeClan getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        getCommand("clan").setExecutor(new DeClanCommand(this));
        getServer().getPluginManager().registerEvents(new DeClanListener(this), getInstance());

        if (!(new File(getDataFolder(), "config.yml").exists())) {
            saveDefaultConfig();
            getLogger().info("New config.yml was made.");
        }
    }

    @Override
    public void onDisable() {
        instance = null;
    }
}